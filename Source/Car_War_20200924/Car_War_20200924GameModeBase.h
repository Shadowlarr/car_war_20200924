// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Car_War_20200924GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CAR_WAR_20200924_API ACar_War_20200924GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
